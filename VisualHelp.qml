import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.12

Item {
    id: root
    anchors.fill: parent
    visible: false
    layer.enabled: true
    z: 9999999

    property Item targetObject
    property alias title: descTitle.text
    property alias text: descText.text
    property alias helpX: helpLabel.x
    property alias helpY: helpLabel.y
    property alias helpHeight: helpLabel.height
    property alias helpWidth: helpLabel.width

    property bool isEditable: false


    onTargetObjectChanged:
        if (targetObject) {
            var coords = targetObject.parent.mapToItem(root.parent, targetObject.x, targetObject.y);
            image.x = coords.x
            image.y = coords.y
            image.width = targetObject.width
            image.height = targetObject.height
            //targetObject.grabToImage(function(result){ image.source = result.url; });
        }

    Rectangle {
        id: backgrowndRect
        anchors.fill: parent
        color: Qt.rgba(0,0,0,.85)

        ShaderEffectSource {
            id: image
            sourceItem: targetObject
            visible: root.parent != targetObject
        }

        Rectangle {
            id: helpLabel
            color: isEditable?Qt.rgba(255,0,0,.4):"transparent"
            border.width: 5
            border.color: isEditable?"green":"transparent"

            Column {
                anchors.fill: parent
                spacing: 15
                Text {
                    id: descTitle
                    width: parent.width
                    color: "white"
                    font.pixelSize: 30
                    font.family: "Roboto"
                    font.bold: true
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
                Text {
                    id: descText
                    width: parent.width
                    color: "white"
                    font.pixelSize: 30
                    font.family: "Roboto"
                    font.bold: true
                    horizontalAlignment: Text.AlignLeft
                    wrapMode: Text.WordWrap
                }
            }

        }

        Rectangle {
            id: transparentForegrownd
            anchors.fill: parent
            color: "transparent"
            MouseArea {
                anchors.fill: parent
                // for editor mode
                property int startX
                property int startY
                onClicked: UIHelper.nextStep();
                onPressAndHold: if ( ! root.isEditable) UIHelper.stopHelp();
                onPressed: {
                    if (isEditable) {
                        startX = mouseX
                        startY = mouseY
                    }
                }
                onPositionChanged: {
                    if (isEditable) {
                        helpLabel.x += mouseX - startX
                        helpLabel.y += mouseY - startY
                        startX = mouseX
                        startY = mouseY
                    }
                }
                onReleased: {
                    if (isEditable)
                        console.log("Label coords: "  + helpLabel.x, helpLabel.y);
                }
            }
        }
    }
}
