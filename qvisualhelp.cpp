#include "QVisualHelp.h"
#include <QJsonDocument>
#include <QJsonParseError>
#include <QDeadlineTimer>
#include <QDebug>

QVisualHelp::QVisualHelp(QQmlApplicationEngine *parent): QObject(parent)
{
    parent->rootContext()->setContextProperty("UIHelper",this);
    setCustomHelpComponent();// load default qml from res
}

void QVisualHelp::addHelpItem(const QString &qmlObjectName, const QString &title, const QString &text, const QJsonObject &additionalInfo)
{
    if (qmlObjectName.isEmpty()) return;
    if (title.isEmpty()) return;
    if (text.isEmpty()) return;

    QJsonObject newItem{
        {"objectName", qmlObjectName},
        {"title"     , title},
        {"text"      , text},
        {"props", additionalInfo}};
    helpItems.append(newItem);
}

void QVisualHelp::loadFromFile(const QString &helpJsonFile)
{
    QFile jFile(helpJsonFile);
    if (jFile.exists()) {
        jFile.open(QIODevice::ReadOnly);
        QJsonParseError jParseErr;
        QJsonDocument jDoc = QJsonDocument::fromJson(jFile.readAll(), &jParseErr);
        jFile.close();
        if (jParseErr.error == QJsonParseError::NoError) {
            if (jDoc.isArray())
                loadFromJson(jDoc.array());
        }
        else qDebug() << "QVisualHelp: Can't parse json file.";
    }
    else qDebug() << "QVisualHelp: Can't open json file.";
}

void QVisualHelp::loadFromJson(const QJsonArray &helpJsArr)
{
    helpItems.clear();
    for (auto jArrEl: helpJsArr) {
        if (!jArrEl.isObject())
            continue;
        QJsonObject jObj = jArrEl.toObject();
        QStringList _keys = jObj.keys();
        if (_keys.contains("objectName") && _keys.contains("title") && _keys.contains("text"))
            helpItems.append(jObj);
    }
}

void QVisualHelp::showHelp()
{
    helpIterator = helpItems.begin();
    getHelpItem();
    updateHelpParams();
}

void QVisualHelp::nextStep()
{
    ++helpIterator;
    if (helpIterator == helpItems.end()) {
        stopHelp();
        return;
    }
    auto helper = getHelpItem();
    Q_ASSERT(helper);
    if (!updateHelpParams() && helpIterator != helpItems.end())
        return nextStep();
}

void QVisualHelp::stopHelp()
{
    helpIterator = helpItems.end();
    updateHelpParams();
}

QQuickItem *QVisualHelp::getHelpItem()
{
    if (helpItem != nullptr)
        return helpItem;
    QQmlApplicationEngine *engine = qobject_cast<QQmlApplicationEngine*>(parent());
    if (engine->rootObjects().isEmpty()) {
        qDebug() << Q_FUNC_INFO << "rootObject is empty!";
        return nullptr;
    }
    QQuickItem *quickRootItem = engine->rootObjects().first()->findChild<QQuickItem*>(rootQuickObjectName);
    if (!quickRootItem) {
        qDebug() << Q_FUNC_INFO << "Can't get root object! (QQuickItem*)";
        return nullptr;
    }
    QQmlComponent *componentLoader = new QQmlComponent(engine, QUrl(helpComponentUrl), this);

    QDeadlineTimer dltimer;
    dltimer.setDeadline(4000);
    while (componentLoader->status() != QQmlComponent::Ready) {
        if (dltimer.remainingTime())
            return nullptr;
        qApp->processEvents();
    }

    helpItem = qobject_cast<QQuickItem*>(componentLoader->create());
    helpItem->setParentItem(quickRootItem);
    if ( helpItem == nullptr)
        qDebug() << "Can't create help component!" << componentLoader->errorString();
    return helpItem;
}

QQuickItem *QVisualHelp::findItemByName(const QString &quickItemObjectName)
{
    QQmlApplicationEngine *engine = qobject_cast<QQmlApplicationEngine*>(parent());
    Q_ASSERT(engine);
    if (engine->rootObjects().isEmpty() || quickItemObjectName.isEmpty())
        return nullptr;
    return engine->rootObjects().first()->findChild<QQuickItem*>(quickItemObjectName);
}

bool QVisualHelp::updateHelpParams()
{
    auto helper = helpItem;
    if (!helper)
        return false;

    if (helpIterator == helpItems.end()) {
        helper->setProperty("title", "");
        helper->setProperty("text", "");
        helper->setVisible(false);
        return true;
    }

    QQuickItem * nextItem = findItemByName(helpIterator->value("objectName").toString());
    if (!nextItem)
        return false;

    if (!nextItem->isVisible()) // show only visible items
        return false;

    helper->setProperty("helpX", helper->width()/3);
    helper->setProperty("helpY", helper->height()/3);
    helper->setProperty("helpWidth", helper->width()/2);
    helper->setProperty("helpHeight", helper->height()/2);

    if (helpIterator->value("props").isObject()) {
        QJsonObject props = helpIterator->value("props").toObject();
        for (auto key: props.keys()) {
            if (key.startsWith("x"))
                helper->setProperty("helpX", props[key].toVariant());
            else
                if (key.startsWith("y")) helper->setProperty("helpY", props[key].toVariant());
            else
                if (key.startsWith("height")) helper->setProperty("helpHeight", props[key].toVariant());
            else
                if (key.startsWith("width")) helper->setProperty("helpWidth", props[key].toVariant());
            else
                helper->setProperty(key.toLocal8Bit(), props[key].toVariant());
        }
    }

    helper->setProperty("targetObject", QVariant::fromValue(nextItem));
    helper->setProperty("title", helpIterator->value("title"));
    helper->setProperty("text", helpIterator->value("text"));
    helper->setVisible(true);
    return true;
}
