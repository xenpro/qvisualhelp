QT += quick qml

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

HEADERS += \
    $$PWD/QVisualHelp.h

SOURCES += \
    $$PWD/qvisualhelp.cpp

DISTFILES +=

RESOURCES += \
    $$PWD/visualhelp.qrc
