#ifndef QVISUALHELP_H
#define QVISUALHELP_H


#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QQmlContext>
#include <QQuickItem>
#include <QJsonObject>
#include <QJsonArray>

class QVisualHelp: public QObject
{
    Q_OBJECT
public:
    QVisualHelp(QQmlApplicationEngine *parent);
    Q_INVOKABLE void addHelpItem(const QString &qmlObjectName,
                     const QString &title,
                     const QString &text,
                     const QJsonObject &additionalInfo = QJsonObject());

    void loadFromFile(const QString &helpJsonFile);
    void loadFromJson(const QJsonArray &helpJsArr);

    void setRootQuickObjectName(const QString &objName) { rootQuickObjectName = objName; }
    void setCustomHelpComponent(const QUrl &qmlUrl = QUrl("qrc:/VisualHelp.qml")) { if (qmlUrl.isValid()) helpComponentUrl = qmlUrl; }

public:
    Q_INVOKABLE void showHelp();
    Q_INVOKABLE void nextStep();
    Q_INVOKABLE void stopHelp();


private:
    QQuickItem *getHelpItem();
    QQuickItem *findItemByName(const QString &quickItemObjectName);
    bool updateHelpParams();

private:
    QUrl helpComponentUrl;
    QString rootQuickObjectName;
    QQuickItem *helpItem{nullptr};
    QList<QJsonObject> helpItems;
    QList<QJsonObject>::iterator helpIterator;
};

#endif // QVISUALHELP_H
